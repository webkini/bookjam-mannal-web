var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/* additional modeules */
var session = require('express-session');
var device = require('express-device');

/* middle-ware */
var boot = require('./controllers/boot');

/* routes */
var routes = require('./routes/index'),
    users = require('./routes/users'),
    catalogs = require('./routes/catalogs'),
    render = require('./routes/render');

var app = express();

/* session setup */
app.use(session({
   secret: 'bookjam-mannal'
  ,resave: true
  ,saveUninitialized: true
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(device.capture());

// device helper setup
device.enableDeviceHelpers(app);
device.enableViewRouting(app);

app.use(boot.preload);

app.use('/', routes);
app.use('/users', users);
app.use('/catalogs', catalogs);
app.use('/r', render);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
