/* routes/catalogs.js */

var express = require('express');
var router = express.Router();
var Catalogs = require('../controllers/catalogs');

//-----------------------------------------------------------------------------

router.get('/images/:filename', Catalogs.market.images);
router.get('/covers/:item_id', Catalogs.market.covers);
router.post('/pages/:itemId', Catalogs.getPageBase64);

router.get('/banners/:id', Catalogs.db.banners);
router.post('/showcases', Catalogs.db.showcases);
router.post('/category', Catalogs.db.category);
router.post('/series/metadata', Catalogs.db.series);

router.get('/series/:series_id', Catalogs.series.list);
router.post('/series/meta', Catalogs.series.meta);
router.get('/series/:series_id/items', Catalogs.series.items);
router.post('/series/search', Catalogs.series.search);

router.post('/items/meta', Catalogs.items.meta);
router.post('/items/files', Catalogs.items.files);
router.post('/serial/meta', Catalogs.items.serial);

router.post('/products', Catalogs.getProductsInfo);

module.exports = router;
