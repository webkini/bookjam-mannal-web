/* routes/index */

var express = require('express');

var router = express.Router();

var	Utils = require('../libs/utils'),
    Users = require('../controllers/users');

//-----------------------------------------------------------------------------

/* routers for views */

router.get('/', function(req, res, next) {
    //res.render('soon');
	res.render('index');
});

router.get('/main', function(req, res, next){ res.render('index'); });

/* users related */

router.get('/login', function(req, res, next) {
	res.render('users/login', { 'rurl': req.query.rurl || '' });
});

router.get('/register', function(req, res, next) {
	res.render('users/register');
});

router.get('/password', function(req, res, next) {
	res.render('users/password');
});

router.get('/bookshelf', function(req, res, next) {
	res.render('users/bookshelf');
});

router.get('/charge', function(req, res, next) {
	res.render('users/charge');
});

router.get('/charge/forward', Users.products.chargeForward_);
router.get('/adult/forward', Users.adult.verifyForward_);

router.get('/coupon', function(req, res, next) {
  res.render('users/coupon', { 'code': req.query.code || '' });
});

router.get('/receipts', function(req, res, next) {
	res.render('users/receipts');
});

router.get('/support', function(req, res, next) {
	res.render('users/support');
});

/* catalogs related */

// series
router.get('/series/:id', function(req, res, next) {
	var showcase_id = req.params.id;
	res.render('series/list', { showcase_id: showcase_id, active_menu: showcase_id });
});

router.get('/series/:id/detail', function(req, res, next) {
	var series_id = req.params.id;
	res.render('series/detail', { series_id: series_id });
});

router.get('/category/:showcase/:category', function(req, res, next) {
	res.render('series/category', { showcase: req.params.showcase, category: req.params.category });
});

router.get('/serial/:showcase/:serial', function(req, res, next) {
	res.render('showcase/serial', { showcase: req.params.showcase, serial: req.params.serial });
});

// prime
router.get('/prime', function(req, res, next) {
    res.render('prime/list', { active_menu: 'prime' });
});
router.get('/prime/:id/detail', function(req, res, next) {
	res.render('prime/detail', { series_id: req.params.id, group_id: req.query.group, active_menu: 'prime' });
});

// everyday
router.get('/everyday', function(req, res, next) {
	res.render('everyday/list', { active_menu: 'everyday' });
});
router.get('/everyday/:id/detail', function(req, res, next) {
	res.render('everyday/detail', {
    series_id: req.params.id,
    banner: req.query.banner,
    orderby: req.query.orderby || 'ASC',
    active_menu: 'everyday'
  });
});

// ranking
router.get('/ranking/:id', function(req, res, next) {
	res.render('ranking/list', { ranking_id: 'ranking.' + (req.params.id || 'weekly'), active_menu: 'ranking' });
});

// theme
router.get('/theme', function(req, res, next) {
	res.render('theme/list', { series_id: req.params.id });
});

router.get('/theme/:id', function(req, res, next) {
	res.render('theme/detail', { series_id: req.params.id });
});

// event
router.get('/event/page/:id', function(req, res, next) {
	res.render('event/pages/'+req.params.id, { display_unit: req.params.id });
});

router.get('/event/:id', function(req, res, next) {
	res.render('event/list', { display_unit: req.params.id });
});

/* docs */

router.get('/docs/contract', function(req, res, next) {
	res.render('users/docs/service-contract');
});

router.get('/docs/private', function(req, res, next) {
	res.render('users/docs/private-info');
});

// notice

router.get('/notice', function(req, res, next) {
	res.render('notice/list');
});

/* viewer */

router.get('/comic/view', function(req, res, next) {
    res.render('view', { item_id: '' });
});

router.get('/comic/:id', function(req, res, next) {
    res.render('view', { item_id: req.params.id });
});

module.exports = router;
