/* routes/render */

var express = require('express'),
	session = require('express-session');

var router = express.Router();

//-----------------------------------------------------------------------------

router.use(function(req, res, next) {
	next();
});

router.post('/:dir/:template', function(req, res, next) {
	var dir  = req.params.dir,
		tmpl = req.params.template;
	res.render('r/'+ dir +'/'+ tmpl, {"data": req.body.data});
});

module.exports = router;
