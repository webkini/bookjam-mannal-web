/* routes/users */

var express = require('express'),
    session = require('express-session');

var router = express.Router();

var	Utils = require('../libs/utils'),
    Users = require('../controllers/users');

//-----------------------------------------------------------------------------

router.post('/login', Users.log.in_);
router.get('/logout', Users.log.out_);
router.delete('/logout', Users.log.out_);

router.put('/token', Users.token.firebase_);

router.get('/points', Users.points.get_);
router.post('/points', Users.points.get_);
router.get('/points/charged', Users.points.charged_);
router.get('/points/:id', Users.points.geta_);

router.get('/', Users.getInfo_);
router.get('/adult', Users.adult.verify_);
router.post('/adult/view', Users.adult.setViewToggle_);
router.post('/register', Users.register_);
router.delete('/password', Users.resetPassword_);
router.put('/support', Users.requestSupport_);
router.delete('/secession', Users.secession_);
router.post('/resend', Users.resendActivator_);

router.get('/items', Users.items.have_);
router.get('/items/:id', Users.items.action_); // no use
router.post('/items/:id', Users.items.postAction_);  // no use
router.get('/items/recents/:id', Users.items.recents_);  // no use

router.post('/purchases', Users.products.receipts_);
router.post('/buy', Users.products.buy_);
router.post('/buys', Users.products.buys_);

router.get('/favorites/:series', Users.favorites.get_);
router.post('/favorites/:series', Users.favorites.add_);
router.delete('/favorites/:series', Users.favorites.remove_);

router.post('/event/:id', Users.submitEvent);
router.put('/coupon/:code', Users.registerCoupon);

router.get('/naver/cb', function(req, res, next) { res.render('users/naver'); });

module.exports = router;
