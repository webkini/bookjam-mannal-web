/* libs/utils */

var os = require('os'),
	fs = require('fs'),
	path = require('path'),
	md5 = require('js-md5');

module.exports = {

	// request & response methods

	getResponse: function(body) {
		try {
			body = (typeof body === 'string') ? JSON.parse(body) : body;
			if (!body.code) body.code = 200;
			return body;
		} catch(e) {
			return null;
		}
	},

	getRequestHeaders: function(app_config, user_headers) {
		return this.mergeObjects({
			'Content-Type': 'application/json',
			'App-Identifier': app_config.id,
			'App-Version': app_config.version,
			'Device-Identifier': 'http-node://anonymous',
			'Device-Type': 'ipad-retina',
			'OS-Version': os.platform() + '_' + os.release(),
			'SBML-Version': app_config.sbml_version,
			'Domain-Key': '',
			'Session-Key': '',
			'Device-Timestamp': Math.floor(Date.now() / 1000),
			'Access-Token': ''
		}, user_headers);
	},

	getDeviceID: function(req) {
		var device_id = 'http-node://anonymous',
			device_id = (req) ? 'http-node://' + req.ip : device_id,
			device_id = (req.session.user) ? 'http-node://' + md5(req.session.user.id) : device_id;
		return device_id;
	},

	// data type & string methods

	isSet: function(mixed) {
		return (typeof mixed === 'undefined') ? false : typeof(mixed);
	},

	mergeObjects: function(obj1, obj2) {
		for (var key in obj2) obj1[key] = obj2[key];
		return obj1;
	},

	uniqueArray: function(arr) {
		var u={},a=[];
		for (var i=0,l=arr.length;i<l;++i) {
			if(u.hasOwnProperty(arr[i])) continue;
      		a.push(arr[i]);
      		u[arr[i]] = 1;
   		}
	   return a;
	},

	textWrap: function(txt, wrapper) {
		return wrapper + txt + wrapper;
	},

	zeroPad: function(number, size) {
		var s = number + "";
		while (s.length < size) s = "0" + s;
		return s;
	},

	arrayToInstring: function(arr) {
		var str_ids = [];
		arr.forEach(function(a){ str_ids.push("'" + a + "'"); });
		return str_ids.join(',');
	},

	// file methods

	rmDirSync: function(dir) {
		var list = fs.readdirSync(dir);
		for (var i=0; i < list.length; i++) {
			var filename = path.join(dir, list[i]),
				stat = fs.statSync(filename);
			if (filename == "." || filename == "..") { /* pass */ }
			else if (stat.isDirectory()) { remove_dir(filename); }
			else { fs.unlinkSync(filename); }
		}
		fs.rmdirSync(dir);
	}
}
