/* libs/cloud */
var request = require('request');

module.exports = {
	request: function(req, api, method, headers, body, callback) {
		if ((!body) || (method === 'GET')) body = '';
		request({
			url: req.CONFIG.host.api.cloud + api,
			method: method,
			headers: headers,
			json: body
		}, callback);
	}
}
