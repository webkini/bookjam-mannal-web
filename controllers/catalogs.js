/* controllers/catalogs */

var request = require('request');
var uurl    = require('url');
var iconv   = require('iconv-lite');

var UTILS   = require('../libs/utils');
var CLOUD   = require('../libs/cloud');
var MARKET  = require('../libs/market');

const PHONE_SUFFIX  = 'iphone-retina6';
const TABLET_SUFFIX = 'ipad-retina';

var self = module.exports = {
    //- market
    market: {
        //= images
        images: function(req, res, next) {
            var device_type = (req.device.type == 'phone') ? PHONE_SUFFIX : TABLET_SUFFIX;
            MARKET.request(req, '/images/' + req.params.filename, 'POST',
                UTILS.getRequestHeaders(req.CONFIG.app, {
                    "Device-Identifier": UTILS.getDeviceID(req),
                    "Device-Type": device_type
                }),
                { "catalog_id": "Default" },
                function(err, reso, body) {
                    if (!err) {
                        return res.send(body);
                    } else {
                        console.log(err);
                        return null;
                    }
                }
            );
        },
        //= covers
        covers: function(req, res, next) {
            var device_type = (req.device.type === 'phone') ? PHONE_SUFFIX : TABLET_SUFFIX;
            MARKET.request(req, '/covers/' + req.params.item_id, 'POST',
                UTILS.getRequestHeaders(req.CONFIG.app, {
      				"Device-Identifier": UTILS.getDeviceID(req),
      				"Device-Type": device_type
      			}),
      			'',
      			function(err, reso, body) {
                    if (!err) {
                        var url_info = uurl.parse(body);
                        return res.send(req.CONFIG.host.cds.bxps + url_info.path);
                    } else {
                        console.log(err);
                        return null;
                    }
      			}
      		);
        }
    }, // market
    //- db basic query
    db: {
        //= banners
        banners: function(req, res, next) {
            var banner_id = req.params.id,
                banners = [],
                display_units = [];
            req.CATALOG_DB.all("SELECT attr FROM banners WHERE banner = ?", banner_id, function(err, rows) {
                if (!err) {
                    rows.forEach(function(row){ row.attr = JSON.parse(row.attr); });
                    return res.json({"banners": rows});
                } else {
                    return res.send(err);
                }
            });
        },
        //= showcases
        showcases: function(req, res, next) {
            var _ids = req.body.id || [],
                _showcases = req.body.showcase || [],
                _series = req.body.series || [],
                offset = req.body.offset || 0,
                length = req.body.length || 0,
                sort = req.body.sort || '';

      		var ids_query = (_ids.length > 0) ? ' AND id IN (' + UTILS.arrayToInstring(_ids) + ') ' : '',
      			showcases_query = (_showcases.length > 0) ? ' AND showcase IN (' + UTILS.arrayToInstring(_showcases) + ') ' : '',
      			series_query = (_series.length > 0) ? ' AND series IN (' + UTILS.arrayToInstring(_series) + ') ' : '';

      		var limit_query = (length != 0) ? ' LIMIT ' + offset + ',' + length + ' ' : '',
      			order_query = '';
      		if (sort) {
      			sort = sort.split(',');
      			order_query = ' ORDER BY ' + sort[0] + ' ' + (sort[1] || 'ASC') + ' ';
      		} else {
      			order_query = ' ORDER BY RANDOM() ';
      		}

            var query = "SELECT * FROM showcases WHERE 1 " + ids_query + showcases_query + series_query + order_query + limit_query;
      		req.CATALOG_DB.all(query, function(err, rows) {
                if (!err) {
                    return res.json({"showcases": self.collectRows(rows)});
                } else {
                    return res.send(err);
                }
      		});
        },
        //= category : query showcase_to_category
        category: function(req, res, next) {
            var _ids = req.body.id || [],
                _showcase = req.body.showcase || [],
                _category = req.body.category || [],
                offset = req.body.offset || 0,
                length = req.body.length || 0,
                sort = req.body.sort || '';

      		var ids_query = (_ids.length > 0) ? ' AND id IN (' + UTILS.arrayToInstring(_ids) + ') ' : '',
      		    showcases_query = (_showcase.length > 0) ? ' AND showcase IN (' + UTILS.arrayToInstring(_showcase) + ') ' : '',
      		    category_query = (_category.length > 0) ? ' AND category IN (' + UTILS.arrayToInstring(_category) + ') ' : '';

      		var limit_query = (length != 0) ? ' LIMIT ' + offset + ',' + length + ' ' : '',
      		    order_query = '';
      		if (sort) {
      			sort = sort.split(',');
      			order_query = ' ORDER BY ' + sort[0] + ' ' + (sort[1] || 'ASC') + ' ';
      		} else {
      			order_query = ' ORDER BY RANDOM() ';
      		}

            var query = "SELECT * FROM showcase_to_category WHERE 1 " + ids_query + showcases_query + category_query + order_query + limit_query;
      		req.CATALOG_DB.all(query, function(err, rows) {
                if (!err) {
        			return res.json({"categories": rows});
                } else {
                    return res.send(err);
                }
      		});
        },
        //= series
        series: function(req, res, next) {
            var series_ids = req.body.series;
            var query = "SELECT id, attr FROM series WHERE id IN (" + UTILS.arrayToInstring(series_ids) + ")";
            req.CATALOG_DB.all(query, function(err, rows){
                if(!err) {
                    var series = {};
                    rows.forEach(function(row) {
                        series[row.id] = JSON.parse(row.attr);
                    });
                    return res.json(series);
                } else {
                    return res.send(err);
                }
            });
        }
    }, // db
    //- series
    series: {
        //= list
        list: function(req, res, next) {
            var series_id = req.params.series_id;
      		req.CATALOG_DB.get("SELECT A.attr AS attr, B.cnt AS cnt FROM \
      		(SELECT attr FROM showcases WHERE showcase = 'series' AND series = ?) AS A, \
      		(SELECT series, COUNT(*) as cnt FROM showcases WHERE showcase = 'books' GROUP BY series HAVING series = ?) AS B", [series_id, series_id], function(err, row){
                    if (!err) {
                        var showcase_attr = JSON.parse(row.attr);
                        showcase_attr.total_volume = row.cnt;
                        MARKET.request(req, '/series/'+series_id, 'GET',
                            UTILS.getRequestHeaders(req.CONFIG.app, {
                                "Device-Identifier": UTILS.getDeviceID(req)
                            }),
                            '',
                            function(err, reso, body) {
                                if (!err) {
                                    var market_meta = JSON.parse(body),
                                    series_info = UTILS.mergeObjects(showcase_attr, market_meta.series[series_id]);
                                    return res.json(series_info);
                                } else {
                                    return res.send(err);
                                }
                            }
                        );
                    } else {
                        return res.send(err);
                    }
      		});
        },
        //= meta
        meta: function(req, res, next) {
            var series_ids = req.body.series;
            MARKET.request(req, '/series', 'POST',
                UTILS.getRequestHeaders(req.CONFIG.app, {
                    "Device-Identifier": UTILS.getDeviceID(req)
                }),
                {"series": series_ids},
                function(err, reso, body) {
                    if (!err) return res.json(body);
                    else return res.send(err);
                }
            );
        },
        //= items
        items: function(req, res, next) {
            var series_id = req.params.series_id,
                sort = req.query.sort || 'oldest';
            req.CATALOG_DB.all("SELECT attr FROM showcases WHERE showcase = 'books' AND series = ?", series_id, function(err, rows){
                if (!err) {
                    var item_attrs = [], item_ids = [];
                    rows.forEach(function(row) {
                        var attr = JSON.parse(row.attr);
                        item_attrs[attr.item] = attr;
                        item_ids.push(attr.item);
                    });
                    MARKET.request(req, '/items', 'POST',
                        UTILS.getRequestHeaders(req.CONFIG.app, {
                            "Device-Identifier": UTILS.getDeviceID(req)
                        }),
                        {"items" : item_ids},
                        function(err, reso, body) {
                            if (!err) {
                                var metas = (body.items) ? body.items : {},
                                items = [];
                                for (var id in metas) items.push(UTILS.mergeObjects(item_attrs[id], metas[id]));
                                if (sort == 'latest') {
                                    items = items.sort(function(a,b){ return (a.id<b.id) ? 1 : (a.id>b.id) ? -1 : (parseInt(b.volume)-parseInt(a.volume)); });
                                } else {
                                    items = items.sort(function(a,b){ return (a.id<b.id) ? -1 : (a.id>b.id) ? 1 : (parseInt(a.volume)-parseInt(b.volume)); });
                                }
                                return res.json({"items": items});
                            } else {
                                return res.send(err);
                            }
                        }
                    );
                } else {
                    return res.send(err);
                }
      		});
        },
        //= search
        search: function(req, res, next) {
            var genre = req.body.genre,
                keyword = req.body.keyword;

            var genre_query = (genre) ? " AND category = '"+ genre +"' " : "",
                query = "SELECT attr FROM showcases " +
                    " WHERE id IN (SELECT id FROM showcase_to_category WHERE showcase = 'series' "+ genre_query +") " +
                    " AND showcase = 'series' AND attr LIKE '%"+ keyword +"%' " +
                    " ORDER BY title ASC ";

            req.CATALOG_DB.all(query, function(err, rows){
                if (!err) {
                    var series =[];
                    rows.forEach(function(row){
                        var attr = JSON.parse(row.attr);
                        attr['series'] = row.id;
                        series.push(attr);
                    });
                    return res.json({"series": series});
                } else {
                    return res.send(err);
                }
            });
        }
    },
    //- items
    items: {
        //= meta
        meta: function(req, res, next) {
            var item_ids = req.body.items;
            MARKET.request(req, '/items', 'POST',
                UTILS.getRequestHeaders(req.CONFIG.app, {
                    "Device-Identifier": UTILS.getDeviceID(req)
                }),
                {"items": item_ids},
                function(err, reso, body) {
                    if (!err) {
                        return res.json(body);
                    } else {
                        return res.send(err);
                    }
                }
            );
        },
        //= files
        files: function(req, res, next) {
            var file_url = req.body.url;
            request({ url: file_url, encoding: null }, function(err, reso, body){
                if (!err) {
                    return res.send(iconv.decode(body, 'iso-8859-1'));
                } else {
                    return res.send(err);
                }
            });
        },
        //= serial
        serial: function(req, res, next) {
            var item_ids = req.body.items;
            var query = "SELECT item, attr FROM showcases WHERE showcase = 'serial' AND item IN (" + UTILS.arrayToInstring(item_ids) + ")";
            req.CATALOG_DB.all(query, function(err, rows){
                if (!err) {
                    var items = {};
                    rows.forEach(function(row){
                        items[row.item] = JSON.parse(row.attr);
                    });
                    return res.json({"items":items});
                } else {
                    res.send(err);
                }
            });
        }
    },


    getProductsInfo: function(req, res, body) {
        var pids = req.body.products;
        CLOUD.request(req, '/products', 'POST',
            UTILS.getRequestHeaders(req.CONFIG.app, {
                "Device-Identifier": UTILS.getDeviceID(req)
            }),
            {"products": pids},
            function(err, reso, body) {
                if (!err) {
                    return res.json({"products": body.products});
                } else {
                    return res.send(err);
                }
            }
        );
    },

    getPageBase64: function(req, res, body) {
      var itemId = req.params.itemId;
      var page_name = req.body.pageName || '';
      if (page_name == '') return res.send('error#001');
      // check preview / login / purchased
      var page_url = req.CONFIG.host.cds.pages + '/' + itemId + '/Images/' + page_name;

      request({ url: page_url, encoding: null }, function(err, reso, body){
          if (!err && reso.statusCode == 200) {
            var data = '';
            data = "data:" + reso.headers["content-type"] + ";base64,"
            data += new Buffer(body).toString('base64');
            return res.send(data);
          } else {
            return res.send(err);
          }
      });
    },

    /* private methods */

    collectRows: function(rows) {
    	var seq = 1;
    	rows.forEach(function(row){
    		row.attr = JSON.parse(row.attr);
    		row._seq = seq++;
    	});
    	return rows;
    }

} // end of module
