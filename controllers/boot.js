/* controllers/boot : middleware at boot */

var sqlite3 = require('sqlite3').verbose();

var CONFIG = require('../config/default');
var UTILS  = require('../libs/utils');
var USERS  = require('./users');

exports.preload = function(req, res, next) {

  req.CONFIG = CONFIG;

  if (!req.session.user)
    if (req.cookies.access_token)
      return USERS.log.restore(req, res, next);

  req.CATALOG_DB = new sqlite3.Database(CONFIG.db.catalog);

	res.locals.user = req.session.user;
  res.locals.config = CONFIG;

	next();

}
