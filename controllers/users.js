/* controllers/users */

var request = require('request');
var crypto = require('crypto');
var firebase = require('firebase-admin');
var firebaseServiceAccount = require('../config/firebase.js');

var UTILS = require('../libs/utils');
var CLOUD = require('../libs/cloud');
var MARKET = require('../libs/market');

firebase.initializeApp({
  credential: firebase.credential.cert(firebaseServiceAccount),
  databaseURL: "https://bookjam-mannal-74c85.firebaseio.com"
});

var self = module.exports = {
// - session
	session: {
		set: function(req, data) {
			return req.session.user = UTILS.mergeObjects(data, req.session.user);
		},

		get: function(req) {
            //console.log(req.session.user);
            return (req.session.user) ? req.session.user : null;
        },

		remove: function(req) { delete req.session.user; }
	},
// - log
	log: {
        //= cloud login
		in_: function(req, res, next) {
            var channel = req.body.channel, userData = {};

            if (channel == 'bookjam') {
                userData = {
                    'channel': req.body.channel,
                    'email': req.body.email,
                    'password': req.body.password
                }
            } else {
                userData = {
                    'channel': req.body.channel,
                    'external_id': req.body.external_id,
                    'access_token': req.body.access_token,
                    'user_info': req.body.user_info || {}
                }
            }

			CLOUD.request(req, '/login', 'POST',
				UTILS.getRequestHeaders(req.CONFIG.app, {
					"Device-Identifier": UTILS.getDeviceID(req),
					"Device-Type": req.headers['user-agent']
				}),
				userData,
				function(err, reso, body) {
                    if (!err) {
                        var result = UTILS.getResponse(body);
                        console.log(body);
                        if (result.code == 200) {
                            var u = {
                				"id": result.user_id,
                				"key": result.session_key,
                				"name": result.name,
                				"email": result.email
                			};
                            self.session.set(req, u);
                            u['access_token'] = self.token.encrypt(req, u);
                            u['cloud'] = result;
                        } else {
                            u = result;
                        }
                        return res.json(u);
                    }
                    return res.send(err);
				}
			);
		},

		out_: function(req, res, next) {
			var rurl = req.query.rurl || '/';
			self.session.remove(req);
			if (req.method == 'GET') return res.redirect(rurl);
			if (req.method == 'DELETE') return res.json({"error": false});
		},

		restore: function(req, res, next) {
            console.log('----- restore session -----');
            var orl = req.originalUrl;
            var user = JSON.parse(self.token.decrypt(req, req.cookies.access_token));
			CLOUD.request(req, '/'+ user.id +'/info', 'GET',
				UTILS.getRequestHeaders(req.CONFIG.app, {
					"Device-Identifier": UTILS.getDeviceID(req),
					"Device-Type": req.headers['user-agent'],
					"Session-Key": user.key
				}),
				'',
				function(err, resp, body) {
                    if (!err) {
                        var result = UTILS.getResponse(body);
                        var u = {
                            "id": result.user_id,
                            "key": result.session_key,
                            "name": result.name,
                            "email": result.email
                        };
                        self.session.set(req, u);
                        res.redirect(orl);
                    } else {
                        res.clearCookie('access_token');
                    }
                }
            );
        }
	},
// - points
	points: {
        get_: function(req, res, next) {
            if (req.session.user) {
                self.points.request(req, function(pts){
                    delete req.session.user.points;
                    self.session.set(req, {"points": pts.points});
                    return res.json({"points": pts.points});
                });
            } else {
                req.session.user.points = [];
                return res.json({"points":[]});
            }
        },

        geta_: function(req, res, next) {
            var point_id = req.params.id;
            if (req.session.user) {
                CLOUD.request(req, '/points/' + point_id, 'GET',
                    UTILS.getRequestHeaders(req.CONFIG.app, {
                        "Device-Identifier": UTILS.getDeviceID(req),
                        "Device-Type": req.headers['user-agent'],
                        "Session-Key": req.session.user.key
                    }),
                    '',
                    function(err, reso, body) {
                        if (!err) {
                            var result = UTILS.getResponse(body);
                            return res.json(result);
                        } else {
                            return res.send(err);
                        }
                    }
                );
            }
        },

		set: function(req, body) {
			if (!req.session.user) return null;
			req.session.user.points = body.points;
			return body.points;
		},

        request: function(req, callback) {
            if (req.session.user) {
                CLOUD.request(req, '/'+req.session.user.id+'/points', 'GET',
                    UTILS.getRequestHeaders(req.CONFIG.app, {
                        "Device-Identifier": UTILS.getDeviceID(req),
                        "Device-Type": req.headers['user-agent'],
                        "Session-Key": req.session.user.key
                    }),
                    '',
                    function(err, reso, body) {
                        if (!err) {
                            var result = UTILS.getResponse(body);
                            self.points.set(req, result);
                            if (callback) return callback(result);
                        } else {
                            return res.send(err);
                        }
                    }
                );
            }
        },

        charged_: function(req, res, next) {
            var point_id = req.query.points_id || null,
                location = req.query.offset || 0,
                length = req.query.length || 30;
            var query = "?location=" + location + "&length=" + length + "&filter=charge";
            CLOUD.request(req, '/logs/points/' + point_id + query, 'GET',
                UTILS.getRequestHeaders(req.CONFIG.app, {
                    "Device-Identifier": UTILS.getDeviceID(req),
                    "Device-Type": req.headers['user-agent'],
                    "Session-Key": req.session.user.key
                }),
                '',
                function(err, reso, body) {
                    if (!err) {
                        var result = UTILS.getResponse(body);
                        return res.json(result);
                    } else {
                        return res.send(err);
                    }
                }
            );
        }
	},
// - token
	token: {
        firebase_: function(req, res, next) {
            var uid = req.body.uid,
                email = req.body.email,
                name = req.body.name;

            var additionalClaims = {
                "email": req.body.email,
                "name": req.body.name
            };

            firebase.auth().createCustomToken(uid, additionalClaims)
                .then(function(token){
                    return res.json({"token": token});
                })
                .catch(function(error){
                    console.log('firebase_', error);
                    return;
                });
        },

        encrypt: function(req, u) {
                var token = null;
                if (u.id && u.key) {
                var cipher = crypto.createCipher(req.CONFIG.auth.algorithm, req.CONFIG.auth.password);
                token = JSON.stringify({"id":u.id,"key":u.key,"created_at":Date.now()});
                token = cipher.update(token, 'utf8', 'hex');
                token += cipher.final('hex');
                }
                return token;
        },

		decrypt: function(req, t) {
			var user = null;
			var decipher = crypto.createDecipher(req.CONFIG.auth.algorithm, req.CONFIG.auth.password);
			user = decipher.update(t, 'hex', 'utf8');
			user += decipher.final('utf8');
			return user;
		}
	},
// - items
	items: {
		have_: function(req, res, next) {
			if (req.session.user) {
    			CLOUD.request(req, '/'+req.session.user.id+'/items', 'GET',
    				UTILS.getRequestHeaders(req.CONFIG.app, {
    					"Device-Identifier": UTILS.getDeviceID(req),
    					"Device-Type": req.headers['user-agent'],
    					"Session-Key": (req.session.user) ? req.session.user.key : ''
    				}),
    				'',
    				function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
        					  return res.json(result);
                } else {
                    return res.send(err);
                }
    				}
    		);
      } else {
        return res.json({'code': 200, 'items': []});
      }
		},

		action_: function(req, res, next) {
			var item_id = req.params.id;
			CLOUD.request(req, '/'+req.session.user.id+'/items/'+item_id, 'GET',
				UTILS.getRequestHeaders(req.CONFIG.app, {
					"Device-Identifier": UTILS.getDeviceID(req),
					"Device-Type": req.headers['user-agent'],
					"Session-Key": (req.session.user) ? req.session.user.key : ''
				}),
				'',
				function(err, reso, body) {
                    if (!err) {
                        var result = UTILS.getResponse(body);
    					return res.json(result);
                    } else {
                        return res.send(err);
                    }
				}
			);
		},

		postAction_: function(req, res, next) {
			var item_id = req.params.id;
			CLOUD.request(req, '/'+req.session.user.id+'/items/'+item_id, 'POST',
				UTILS.getRequestHeaders(req.CONFIG.app, {
					"Device-Identifier": UTILS.getDeviceID(req),
					"Device-Type": req.headers['user-agent'],
					"Session-Key": (req.session.user) ? req.session.user.key : ''
				}),
				{"action": "open"},
				function(err, reso, body) {
                    if (!err) {
                        var result = UTILS.getResponse(body);
    					return res.json(result);
                    }  else {
                        return res.send(err);
                    }
				}
			);
		},

		recents_: function(req, res, next) {
			var item_id = req.params.id;
			CLOUD.request(req, '/'+req.session.user.id+'/recents/'+item_id, 'GET',
				UTILS.getRequestHeaders(req.CONFIG.app, {
					"Device-Identifier": UTILS.getDeviceID(req),
					"Device-Type": req.headers['user-agent'],
					"Session-Key": (req.session.user) ? req.session.user.key : ''
				}),
				'',
				function(err, reso, body) {
                    if (!err) {
                        var result = UTILS.getResponse(body);
    					return res.json(result);
                    } else {
                        return res.send(err);
                    }
				}
			);
		}
	},
// - products
	products: {
		buy_: function(req, res, next) {
			var product_id = req.body.product_id || null,
				points_id = req.body.points_id || req.CONFIG.points.basic;
			if (req.session.user) {
				CLOUD.request(req, '/purchases', 'POST',
					UTILS.getRequestHeaders(req.CONFIG.app, {
						"Device-Identifier": UTILS.getDeviceID(req),
						"Device-Type": req.headers['user-agent'],
						"Session-Key": req.session.user.key
					}),
					{ "product_id": product_id, "points_id": points_id },
					function(err, reso, body) {
                        if (err) {
                            var result = UTILS.getResponse(body);
    						return res.json(result);
                        } else {
                            return res.send(err);
                        }
					}
				);
			}
		},

        buys_: function(req, res, next) {
            var product_ids = req.body.product_ids || [],
                points_id = req.body.points_id || req.CONFIG.points.basic;
            if (req.session.user) {
                CLOUD.request(req, '/multi_purchases', 'POST',
                    UTILS.getRequestHeaders(req.CONFIG.app, {
                        "Device-Identifier": UTILS.getDeviceID(req),
                        "Device-Type": req.headers['user-agent'],
                        "Session-Key": req.session.user.key
                    }),
                    { "products": product_ids, "points_id": points_id },
                    function(err, reso, body) {
                        if (!err) {
                            var result = UTILS.getResponse(body);
                            return res.json(result);
                        } else {
                            return res.send(err);
                        }
                    }
                );
            }
        },

        chargeForward_: function(req, res, next) {
            var product_id = req.query.product_id,
                charge_method = req.query.charge_method,
                payment_method = '';

            if (charge_method == 'mobile') payment_method = 'Teledit';
            if (charge_method == 'card') payment_method = 'CreditCard';
            if (charge_method == 'culture') payment_method = 'CultureVoucher';

            var headers = UTILS.getRequestHeaders(req.CONFIG.app, {
                "Device-Identifier": "web_" + UTILS.getDeviceID(req),
                "Device-Type": (req.device.type == 'desktop') ? "web" : "mobile",
                "Session-Key": req.session.user.key,
                "Payment-Method": payment_method,
                "Payment-Environment": (req.CONFIG.debug) ? "sandbox" : "production"
            });

            res.render('partials/users/charge-forward', { "product_id": product_id, "headers": headers});
        },

        receipts_: function(req, res, next) {
            if (req.session.user) {
                CLOUD.request(req, '/'+req.session.user.id+'/purchases?location=0&length=30', 'GET',
                    UTILS.getRequestHeaders(req.CONFIG.app, {
                        "Device-Identifier": UTILS.getDeviceID(req),
                        "Device-Type": req.headers['user-agent'],
                        "Session-Key": req.session.user.key
                    }),
                    '',
                    function(err, reso, body) {
                        var result = UTILS.getResponse(body);
                        var product_ids = [];
                        result.purchases.forEach(function(p){ product_ids.push(p.product_id); });
                        MARKET.request(req, '/products', 'POST',
                            UTILS.getRequestHeaders(req.CONFIG.app, {
                                "Device-Identifier": UTILS.getDeviceID(req),
                                "Device-Type": req.headers['user-agent'],
                                "Session-Key": req.session.user.key
                            }),
                            {"products": product_ids},
                            function(err, reso, body) {
                                if (!err) {
                                    var receipts = [];
                                    var metas = UTILS.getResponse(body);
                                    result.purchases.forEach(function(p){
                                        var pid = p.product_id;
                                        receipts.push(UTILS.mergeObjects(p, {"meta": metas.products[pid]}));
                                    });
                                    return res.json({"receipts": receipts});
                                } else {
                                    return res.send(err);
                                }
                            }
                        );
                    }
                );
            }
        }
	},
// - adult
    adult: {
        verify_ : function(req, res, next) {
            if (req.session.user) {
                if (req.session.user.is_adult) return res.json({'is_adult': req.session.user.is_adult});

                CLOUD.request(req, '/verify/age', 'GET',
                    UTILS.getRequestHeaders(req.CONFIG.app, {
                        "Device-Identifier": UTILS.getDeviceID(req),
                        "Device-Type": req.headers['user-agent'],
                        "Session-Key": req.session.user.key
                    }),
                    '',
                    function(err, reso, body) {
                        if (!err) {
                            var result = UTILS.getResponse(body);
                            delete req.session.user.is_adult;
                            if (result.code == 200) {
                                self.session.set(req, {'is_adult': result.adult});
                                return res.json({'is_adult': result.adult});
                            } else {
                                self.session.set(req, {'is_adult': ''});
                                return res.json({'is_adult': ''});
                            }
                        } else {
                            return res.send(err);
                        }
                    }
                );
            }
        },

        verifyForward_ : function(req, res, next) {
            var headers = UTILS.getRequestHeaders(req.CONFIG.app, {
                "Device-Identifier": "web_" + UTILS.getDeviceID(req),
                "Device-Type": (req.device.type == 'desktop') ? "web" : "mobile",
                "Session-Key": req.session.user.key,
                "Service-Environment": (req.CONFIG.debug) ? "sandbox" : "production"
            });
            res.render('partials/users/adult-forward', {"headers": headers});
        },

        setViewToggle_ : function(req, res, next) {
            delete req.session.user.adult_view;
            self.session.set(req, {'adult_view': req.body.adult_view});
            return res.json({'adult_view': req.body.adult_view});
        }
    },
// - favorites
	favorites: {
		get_: function(req, res, next) {
			var series = req.params.series;
			if (req.session.user) {
				CLOUD.request(req, '/' + req.session.user.id + '/favorites', 'GET',
					UTILS.getRequestHeaders(req.CONFIG.app, {
						"Device-Identifier": UTILS.getDeviceID(req),
						"Device-Type": req.headers['user-agent'],
						"Session-Key": req.session.user.key
					}),
					'',
					function(err, reso, body) {
                        if (!err) {
                            var result = UTILS.getResponse(body);
    						if (series == 'all') return res.json(result);
    						for(key in result) {
    							if (series == key) return res.json(result[key]);
    						}
    						res.json({});
                        } else {
                            return res.send(err);
                        }
					}
				);
			}
		},

		add_: function(req, res, next) {
			var series = req.params.series;
			if (req.session.user) {
				CLOUD.request(req, '/' + req.session.user.id + '/favorites', 'POST',
					UTILS.getRequestHeaders(req.CONFIG.app, {
						"Device-Identifier": UTILS.getDeviceID(req),
						"Device-Type": req.headers['user-agent'],
						"Session-Key": req.session.user.key
					}),
					{"series_id": series},
					function(err, reso, body) {
                        if (!err) {
                            res.json(body);
                        } else {
                            res.send(err);
                        }
                    }
				);
			}
		},

		remove_: function(req, res, next) {
			var series = req.params.series;
			if (req.session.user) {
				CLOUD.request(req, '/' + req.session.user.id + '/favorites', 'DELETE',
					UTILS.getRequestHeaders(req.CONFIG.app, {
						"Device-Identifier": UTILS.getDeviceID(req),
						"Device-Type": req.headers['user-agent'],
						"Session-Key": req.session.user.key
					}),
					{"series_id": series},
					function(err, reso, body) {
                        if(!err) {
                            res.json(body);
                        } else {
                            res.send(err);
                        }
                    }
				);
			}
		}
	},

// @public

	getInfo_: function(req, res, next) {
		return res.json(self.session.get(req));
	},

	register_: function(req, res, next) {
		var user_data = {
			'name': req.body.name,
			'channel': req.body.channel,
			'email': req.body.email,
			'password': req.body.password
		};
		CLOUD.request(req, '/register', 'POST',
			UTILS.getRequestHeaders(req.CONFIG.app, {
				"Device-Identifier": UTILS.getDeviceID(req),
				"Device-Type": req.headers['user-agent']
			}),
			user_data,
			function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
    				return res.json(result);
                } else {
                    return res.send(err);
                }
			}
		);
	},

	secession_: function(req, res, next) {
		CLOUD.request(req, '/' + req.session.user.id + '/secession', 'POST',
			UTILS.getRequestHeaders(req.CONFIG.app, {
				"Device-Identifier": UTILS.getDeviceID(req),
				"Device-Type": req.headers['user-agent'],
				"Session-Key": req.session.user.key
			}),
			{},
			function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
    				return res.json(result);
                } else {
                    return res.send(err);
                }
			}
		);
	},

	resetPassword_: function(req, res, next) {
		var user_data = {
			'email': req.body.email,
			'channel': 'bookjam'
		};
		CLOUD.request(req, '/password/reset', 'POST',
			UTILS.getRequestHeaders(req.CONFIG.app, {
				"Device-Identifier": UTILS.getDeviceID(req),
				"Device-Type": req.headers['user-agent']
			}),
			user_data,
			function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
    				return res.send(result);
                } else {
                    return res.send(err);
                }
			}
		);
	},

    resendActivator_: function(req, res, next) {
        CLOUD.request(req, '/' + req.session.user.id + '/activate', 'POST',
			UTILS.getRequestHeaders(req.CONFIG.app, {
				"Device-Identifier": UTILS.getDeviceID(req),
				"Device-Type": req.headers['user-agent'],
				"Session-Key": req.session.user.key
			}),
			{},
			function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
    				return res.json(result);
                } else {
                    return res.send(err);
                }
			}
		);
    },

	requestSupport_: function(req, res, next) {
		var cs_data = {
			'email': req.body.email,
			'requests': "전화번호:" + req.body.phone + "\n" + req.body.memo
		};
		var user_id = (req.session.user) ? req.session.user.id : 'anonymous';
		CLOUD.request(req, '/'+user_id+'/cs/new', 'PUT',
			UTILS.getRequestHeaders(req.CONFIG.app, {
				"Device-Identifier": UTILS.getDeviceID(req),
				"Device-Type": req.headers['user-agent'],
				"Session-Key": (req.session.user) ? req.session.user.key : ''
			}),
			cs_data,
			function(err, reso, body) {
                if (!err) {
                    var result = UTILS.getResponse(body);
    				return res.send(result);
                } else {
                    return res.send(err);
                }
			}
		);
	},

    submitEvent: function(req, res, next) {
        var event_id = req.params.id;
        MARKET.request(req, '/events/submit', 'POST',
            UTILS.getRequestHeaders(req.CONFIG.app, {
                "Device-Identifier": UTILS.getDeviceID(req),
                "Device-Type": req.headers['user-agent'],
                "Session-Key": (req.session.user) ? req.session.user.key : ''
            }),
            {"event_id": event_id},
            function(err, reso, body) {
                //console.log(reso.statusCode);
                if (reso.statusCode == 200) {
                    var result = UTILS.getResponse(body);
                    return res.send(result);
                } else {
                    return res.json({"code": reso.statusCode});
                }
            }
        );
    },

    registerCoupon: function(req, res, next) {
      var coupon_code = req.params.code || '';
      if (coupon_code == '') return res.send('no code');
      CLOUD.request(req, '/coupons/'+coupon_code, 'PUT',
        UTILS.getRequestHeaders(req.CONFIG.app, {
          "Device-Identifier": UTILS.getDeviceID(req),
          "Device-Type": req.headers['user-agent'],
          "Session-Key": (req.session.user) ? req.session.user.key : ''
        }),
        {
          'app_id': req.CONFIG.app.id,
          'key': 'b7da27bebcbaf25bf698a9042fb8c2b4',
          'tickets': 1,
          'status': 'inuse'
        },
        function(err, reso, body) {
          if (!err) {
            var result = UTILS.getResponse(body);
            return res.send(result);
          } else {
            return res.send(err);
          }
        }
      );
    }

}
