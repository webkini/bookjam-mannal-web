module.exports = {
	"debug": true,

	"app": {
		"id": "kr.co.dcworks.Mannal",
		"version": "0.0.1",
		"sbml_version": "2.0.0"
	},

	"host": {
		"api": {
			"cloud": "https://bookjam-cloud-test.appspot.com/api/v2.2",
			"market": "http://store.bookjam.pe.kr/api/v2"
		},
		"cds": {
			"bxps": "http://bxfs151.bookjam.net:20128",
			"pages": "http://store.bookjam.pe.kr/comics"
		}
	},

	"db": {
		"app": "data/app.sqlite",
		"catalog": "data/catalog.sqlite"
	},

	"points": {
		"basic": "O_0000_000000009",
		"basic-point-name": "코인"
	},

	"cache": {
		"storage": "data/cache.sqlite",
		"max-age": 300
	},

	"image_cache" : {
		"storage": "public/images",
		"max-age": 86400
	},

	"path": {
	},

	"auth": {
		"algorithm": "aes-256-ctr",
		"password": "aksskf16aksskf",
		"cookie": {
			"expires": 0.125,
			"remember_days": 90
		},

		"firebase": {
			"apiKey": "AIzaSyAh1L-udPTuVD-kTduYJbUlMnBOumN2aZM",
			"authDomain": "localhost",
			"databaseURL": "https://bookjam-mannal-74c85.firebaseio.com",
			"storageBucket": "bookjam-mannal-74c85.appspot.com"
		},

		"facebook": {
			"appId": "356145061418942",
			"secret": "c644f79c7f8d27f8fe083d1d6df3cdcd",
			"clientToken": "b4751851cb5119c28f2739a8e74f35a5"
		},

		"naver": {
			"clientId": "zly4k0hofKUJdMSlX88c",
			"callbackUrl": "http://localhost:3031/users/naver/cb",
			"serviceDomain": "localhost"
		}
	}

}
