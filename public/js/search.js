/* public/js/search.js */

$(function(){
	loadGenreSelector($('#search-genre'),'s');

	$('#search-form').on('submit', function(e){
		e.preventDefault();

		var keyword = $('#search-keyword').val();
		var genre   = $('#search-genre-code').val();

		if (genre == '') {
			keyword = /\s*([0-9a-zA-Z!@#$%^&*][^\s]{1}.*|[ㄱ-ㅎ가-힣].*)/g.exec(keyword);
			if (!keyword) {
				mAlert('검색어는 영숫자는 2자,한글은 1자 이상이어야 합니다.');
				$('#search-keyword').focus();
				return false;
			}
			keyword = keyword[0];
		}

		searchObserver(genre, keyword);
	});
});

function changeSearchGenre(code) {
	var label = $('[data-code="'+code+'"]').html();
	$('#search-genre-label').text(label);
	$('#search-genre-code').val(code);
}

function searchObserver(genre, keyword) {
	Catalogs.search(genre, keyword, renderSearchResult);
}

function renderSearchResult(res) {
	var series = res.series,
		genre  = $('#search-genre-label').html();
		result = $('#search-result');

	if (genre != '전체 장르') {
		series = [];
		res.series.forEach(function(s, index){ if (s.genre == genre) series.push(s); });
	}

	if (series.length == 0) {
		result.html('<div class="media text-center"><h4 style="color:#ccc;">결과가 없습니다.</h4></div>');
	} else {
        Catalogs.renderIn($(result), '/r/series/list-s', {'data': series}, function(html) {
            $('#search-result img.cover').lazyload({ effect: "fadeIn", container: $("#search-result") });
        });
	}
}
