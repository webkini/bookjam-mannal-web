/* public/js/catalogs.js */

var Catalogs = new (function() {

// @private

	var _self = this;

	function _request(api, method, body, callback, async) {
		async = async || true;
		body = (!body) ? '' : JSON.stringify(body);
		$.ajax({
			url: api, type: method, dataType: 'JSON', contentType: 'application/json', data: body, async: async,
			success: callback
		});
	}

	function _render(template, method, body, callback, async) {
		async = async || true;
		body = (!body) ? '' : JSON.stringify(body);
		$.ajax({
			url: template, type: method, dataType: 'HTML', contentType: 'application/json', data: body, async: async,
			success: callback
		});
	}

	function _reqbase64(api, method, body, callback, async) {
		async = async || true;
		body = (!body) ? '' : JSON.stringify(body);
		$.ajax({
			url: api, type: method, contentType: 'application/json', data: body, async: async,
			success: callback
		});
	}

	function _mergeObjs(a,b) {
		for (var k in b) a[k] = b[k];
		return a;
	}

// @public

	_self.loadImages = function(els) {
		$(els).each(function(i, el) {
			if (el.tagName != 'IMG') return;
			if ($(el).hasClass('cover') && $(el).attr('data-item')) {
				$.ajax({
					url: '/catalogs/covers/' + $(el).attr('data-item'),
					type: 'GET',
					success: function(res) {
						$(el).attr('data-original', res);
						$(el).lazyload({ effect: "fadeIn" });
					}
				});
			} else
			if ($(el).attr('data-name')) {
				$.ajax({
					url: '/catalogs/images/' + $(el).attr('data-name'),
					type: 'GET',
					success: function(res) {
						$(el).attr('src', res);
					}
				});
			}
		});
	}

	_self.loadPage = function(page_el, item_id, page_name, callback) {
		_reqbase64('/catalogs/pages/' + item_id, 'POST', {'pageName': page_name}, function(res) {
			if (callback) callback(page_el, res);
		});
	}

//-

	_self.banners = function(banner_id, callback) {
		 _request('/catalogs/banners/' + banner_id, 'GET', null, callback);
	}

	_self.showcase = function(showcase, params, callback) {
		var showcase = (showcase) ? {"showcase":[showcase]} : {},
			params = params || {};
		_request('/catalogs/showcases', 'POST', _mergeObjs(showcase, params), callback);
	}

	_self.category = function(category, params, callback) {
		var category = (category) ? {"category":[category]} : {},
			params = params || {};
		_request('/catalogs/category', 'POST', _mergeObjs(category, params), callback);
	}

	_self.seriesMetaData = function(series_ids, callback) {
		_request('/catalogs/series/metadata', 'POST', {'series': series_ids}, callback);
	}

//-

	_self.seriesMeta = function(series_ids, callback){
		_request('/catalogs/series/meta', 'POST', {'series': series_ids}, callback);
	}

	_self.seriesItems = function(series_id, sort, callback) {
		_request('/catalogs/series/'+series_id+'/items?sort='+sort, 'GET', null, callback);
	}

	_self.search = function(genre, keyword, callback) {
		_request('/catalogs/series/search', 'POST', {'genre': genre, 'keyword': keyword}, callback);
	}

//-

	_self.itemsMeta = function(item_ids, callback) {
		_request('/catalogs/items/meta', 'POST', {'items': item_ids}, callback);
	}

	_self.serialMeta = function(item_ids, callback) {
		_request('/catalogs/serial/meta', 'POST', {'items': item_ids}, callback);
	}

// @public but combined

	_self.auxiliary = function(showcase, data, callback) {
		var auxs = [];
		_self.showcase(showcase, data, function(res) {
			var axs = [];
			res.showcases.forEach(function(s) {
				axs.push(s.attr.auxiliary);
				s.attr._seq = s._seq;
				auxs[s.attr.auxiliary] = s.attr;
			});
			_self.showcase(null, {"id": axs}, function(res) {
				var showcases = [];
				res.showcases.forEach(function(s) {
					showcases.push(_mergeObjs(auxs[s.id], s.attr));
				});
				showcases = showcases.sort(function(a,b){return (a._seq>b._seq)?1:-1;});
				callback(showcases);
			});
		});
	}

	_self.renderIn = function(container, template, data, callback, async, flag) {
		_render(template, 'POST', data, function(html) {
			$(container).html(html);
			//_self.setAdultCovers();
			if (!flag) _self.loadImages($(container).find('img'));
			if (callback) callback(html);
		}, (async || true));
	}

// @public helpers

	_self.loadGenre = function(callback) {
		var genres = [];
		_self.showcase('genre', {sort: 'id,ASC'}, callback);
	}

	_self.setAdultCovers = function() {
		$('.adult .cover').removeAttr('data-item').parent().attr('href', '#adult');
	}

});

/* extra interface */

function loadGenreSelector(container, type) {
	Catalogs.loadGenre(function(res){
		Catalogs.renderIn($(container), '/r/genre/list-'+type, {'data': res.showcases});
	});
}

function changeListByGenre(code) {
	var label = $('[data-code="'+ code +'"]').html();
	var items_count = 0;

	$('#genre-label').text(label);
	$('.no-series').hide();

	if (code == '') { // 전체 장르
		$('.items-count').text($('.media .cover').length);
		$('#series-list .media').show();
		$('#series-list .cover').lazyload({ effect: "fadeIn" });
		return;
	}

	$('#series-list .media').each(function(i, s){
		if ($(this).find('.genre').attr('data-genre') == code) {
			$(this).show();
			items_count++;
		} else {
			$(this).hide();
		}
	});
	$('.items-count').text(items_count);
	$('#series-list .cover').lazyload({ effect: "fadeIn" });
	if (items_count == 0) $('.no-series').show();
}

function checkRentalTimer() {
	var now = parseInt(Date.now() / 1000);
	$('.open[data-expired!=""]').each(function(i, b) {
		var remains = ($(this).attr('data-expired') - now) / 3600;
		if (remains < 0) {
			document.location.reload();
		} else {
			remains = (remains > 1) ? Math.floor(remains) + '시간 남음' : Math.floor(remains * 60) + '분 남음'; // 버림
			$(this).text(remains);
		}
	});
}

function sortItemList(order_by) {
	var items = [];
	$('.item').each(function(i,m){ items.push({ 'id': $(this).attr('id') }); });
	if (order_by === 'latest')
		items.sort(function(a, b){ return (a.id < b.id) ? 1 : -1; });
	if (order_by === 'oldest')
		items.sort(function(a, b){ return (a.id > b.id) ? 1 : -1; });
	$.each(items, function(i, item){ $('#'+item.id).detach().appendTo($('#series-items')); });
	$('.item img.cover').lazyload({ effect: "fadeIn" });
}
