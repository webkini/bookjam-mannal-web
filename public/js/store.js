/* public/js/store.js */
var Store = new function(){
	var _self = this, _CART = {}, _BASE_POINT = '';

	function _request(api, method, body, callback, async) {
		async = async || true;
		body = (!body) ? '' : JSON.stringify(body);
		$.ajax({
			url: api, type: method, dataType: 'JSON', contentType: 'application/json', data: body, async: async,
			success: callback
		});
	}

	function _render(template, method, body, callback, async) {
		async = async || true;
		body = (!body) ? '' : JSON.stringify(body);
		$.ajax({
			url: template, type: method, dataType: 'HTML', contentType: 'application/json', data: body, async: async,
			success: callback
		});
	}

	function _mergeObjs(a, b) { for (var k in b) a[k] = b[k]; return a; }

/* @ private methods */

	function _getPaymentInfo(payments, point_id) {
		var payment;
		point_id = point_id || _self._BASE_POINT;
		payments.forEach(function(p){ if (p.points_id == point_id) payment = p; });
		return (payment) ? payment : false;
	}

	function _setTitle(title) {
		$('#modal-purchase').find('.modal-title').html(title);
	}

	function _viewOpenModal(cart) {
		_setTitle('구매 선택');

		var valid_length = Math.max(cart.products.permanent.length, cart.products.rental.length, cart.products.allrental.length)
//var valid_length = cart.products.permanent.length
		   ,valid_products = [];

	   	var purchase_desc = '<h5>'+ cart.title;
	   	purchase_desc += ((cart.mode!='single') && (cart.item_ids.length>1)) ? '외 '+ (valid_length-1) +'건' : ''
   		purchase_desc +=  (cart.mode!='single') ? '(전'+ valid_length +'편)' : '';
   		purchase_desc += '</h5><hr/>';
   		$('#modal-purchase').find('.modal-body').html(purchase_desc);

		for (var k in cart.products) {
			if (cart.products[k].length == valid_length) {
				valid_products = valid_products.concat(cart.products[k]);
			}
		}

		var products_price = {'permanent': 0, 'rental': 0, 'allrental': 0};
		_request('/catalogs/products', 'POST', {'products': valid_products}, function(res) {
			res.products.forEach(function(p){
//console.log(p.payments);
				var payment = _getPaymentInfo(p.payments);
//console.log(payment);
				for (var type in cart.products)
					if (cart.products[type].indexOf(p.product_id) > -1)
						products_price[type] += parseInt(payment.price);
			});

			cart.price_amount = products_price;

			var btn_outs = '';
			for (var type in products_price) {
				if ((cart.mode != 'all') && (type == 'allrental')) continue;
				if ((cart.mode == 'all') && (type == 'rental')) continue;
				if (cart.products[type].length != valid_length) continue;
				if (isNaN(products_price[type])) continue;

				btn_outs += '<div class="row" style="margin-bottom:10px;">';
				if (type == 'permanent') {
					btn_outs += '<div class="col-xs-4"><h5>소장</h5></div>';
				} else {
					btn_outs += '<div class="col-xs-4"><h5>대여' + ((type == 'rental') ? '(3일)' : '(7일)') + '</h5></div>'; // all rental : 7일
				}
				btn_outs += '<div class="col-xs-8">' +
					'<button type="button" class="btn btn-primary btn-block btn-" onclick="Store.confirmPurchase(\''+ type +'\');">' +
					((products_price[type]==0) ? '무료' : (withCommas(products_price[type]) + ' 코인')) +
					'</button></div></div>';
			}
			if (cart.mode != 'single') btn_outs += '<p class="help-block"><small>* 이미 대여/구매한 상품은 포함되지 않습니다.</small></p>';

			$('#modal-purchase').find('.modal-body').append(btn_outs);
			$('#modal-purchase').modal();
		});
	}

	function _viewConfirmModal(type) {
		var cart = _self._CART
		   ,valid_length = cart.products[type].length;

		_setTitle('구매 확인');
//console.log(cart);
		var purchase_desc = '<h5>'+ cart.title;
	   	purchase_desc += ((cart.mode!='single') && (cart.item_ids.length>1)) ? '외 '+ (valid_length-1) +'건' : ''
   		purchase_desc +=  (cart.mode!='single') ? '(전'+ valid_length +'편)' : '';
   		purchase_desc += '</h5>';

		purchase_desc += '<p style="margin-top:10px;">'+ ((cart.price_amount[type] == 0) ? '무료로 ' : withCommas(cart.price_amount[type])+ ' 코인에 ');
		purchase_desc += ((type == 'permanent') ? '소장' : '대여') + ' 하시겠습니까?</p><hr/>';

		purchase_desc += '<div class="row" style="margin-bottom:10px;">';
		purchase_desc += '<div class="col-xs-6"><button type="button" class="btn btn-danger btn-block" onclick="$(\'#modal-purchase\').modal(\'hide\');">취소</button></div>';
		purchase_desc += '<div class="col-xs-6"><button type="button" class="btn btn-primary btn-block" onclick="Store.purchaseItems(\''+ type +'\');">확인</button></div>';
		purchase_desc += '</div>';

		$('#modal-purchase .modal-body').html(purchase_desc);
	}

	function _viewEverydayModal(cart) {
		_setTitle('매일 연재 보기');

		var purchase_desc = '<h4>'+ cart.title +'</h4>';
		$('#modal-purchase').find('.modal-body').html(purchase_desc);

		_request('/catalogs/products', 'POST', {'products': cart.products.rental}, function(res) {
			var btn_outs = '';
			var payments = res.products[0].payments, payment, base_price;

			if (_self._BASE_POINT == 'O_0000_000000009') {
				payment = _getPaymentInfo(payments, _self._BASE_POINT);
				base_price = parseInt(payment.price);

				if (base_price > 0) {
					btn_outs += '<p>최신 30화는 무료이용권으로 볼 수 없습니다. '+ withCommas(base_price) +' 코인으로 대여(24시간)하시겠습니까?</p>';
				} else {
					btn_outs += '<p>무료로 대여(23시간) 하시겠습니까?</p>';
				}
				btn_outs += '<hr/><div class="row" style="margin-bottom:10px;">';
				btn_outs += '<div class="col-xs-6"><button type="button" class="btn btn-danger btn-block" onclick="$(\'#modal-purchase\').modal(\'hide\');">취소</button></div>';
				btn_outs += '<div class="col-xs-6"><button type="button" class="btn btn-primary btn-block" onclick="Store.purchaseItems(\'rental\');">확인</button></div>';
				btn_outs += '</div>';

				$('#modal-purchase').find('.modal-body').append(btn_outs);
				$('#modal-purchase').modal();
				return;
			}

			// _BASE_POINT == FREE_POINT
			Users.cloud.getPointStat(_self._BASE_POINT, function(res){
				var point = _getPaymentInfo(res.points, _self._BASE_POINT);

				if (point.points_info.total_amount > 0) {
					btn_outs += '<p>무료 이용권으로 대여(24시간) 하시겠습니까?</p>';
				} else {
					_self.setBasePoint('O_0000_000000009');
					payment = _getPaymentInfo(payments, 'O_0000_000000009');
					base_price = parseInt(payment.price);

					btn_outs += '<p>무료 이용권 소진으로 충전 코인으로 구매 진행합니다. '+ withCommas(base_price) + ' 코인으로 대여(24시간) 하시겠습니까?</p>';
				}

				btn_outs += '<hr/><div class="row" style="margin-bottom:10px;">';
				btn_outs += '<div class="col-xs-6"><button type="button" class="btn btn-danger btn-block" onclick="$(\'#modal-purchase\').modal(\'hide\');">취소</button></div>';
				btn_outs += '<div class="col-xs-6"><button type="button" class="btn btn-primary btn-block" onclick="Store.purchaseItems(\'rental\');">확인</button></div>';
				btn_outs += '</div>';

				$('#modal-purchase').find('.modal-body').append(btn_outs);
				$('#modal-purchase').modal();
				return;
			});
		});
	}

/* @ public methods */

	_self.setBasePoint = function(point_id) { _self._BASE_POINT = point_id; }

	_self.flushCart = function() { _self._CART = {}; }

	_self.openCart = function(mode, item_ids, base_point, in_viewer) {
		_self._BASE_POINT = base_point || _self._BASE_POINT;
		_self._CART = { 'mode': mode, 'item_ids': item_ids || [], 'in_viewer': in_viewer || false };

		if (mode == 'all') $('input.item-id').prop('checked', true);
		if (mode != 'single') {
			if ($('input.item-id:checked').length < 1) {
				mAlert('선택된 만화가 없습니다.', 'danger', 2000);
				return false;
			}
			$('input.item-id:checked').each(function(e){ _self._CART.item_ids.push($(this).val()); });
		}

		var products = {'permanent':[], 'rental':[], 'allrental':[]};
		var title = '';
		_self._CART.item_ids.forEach(function(id){
			var ps = $('button.purchase[data-item='+ id +']');
			title = (title != '') ? title : ps.data('title');
			for (var k in products) {
				if (ps.data(k)) products[k].push(ps.data(k));
			}
		});
		_self._CART.title = title;
		_self._CART.products = products;
		_viewOpenModal(_self._CART);
	}

	_self.openEveryday = function(item_id, free_point) {
		var $item = $('button.purchase[data-item='+ item_id +']').first();
		var products = {'permanent':[], 'rental':[$item.data('rental')], 'allrental':[]};
		var title = $item.data('title') || '';

		_self._BASE_POINT = free_point || _self._BASE_POINT;
		_self._CART = {'mode': 'signle', 'item_ids': [item_id], 'title': title, 'products': products};
		_viewEverydayModal(_self._CART);
	}

	_self.openViewer = function(item_id) {
		var $item = $('button.purchase[data-item='+ item_id + ']').frist();
		var products = {'permanent':[], 'rental':[$item.data('rental')], 'allrental':[]};
		var title = $item.data('title') || '';

		_self._BASE_POINT = 'O_0000_000000009';
		_self._CART = {'mode': 'single', 'item_ids': [item_id], 'title': title, 'products': products};
		_viewViewerModal(_self._CART);
	}

	_self.confirmPurchase = function(type) { _viewConfirmModal(type); }

	_self.purchaseItems = function(type) {
		var cart = _self._CART;
		var products = cart.products[type];

		Users.buyItems(products, _self._BASE_POINT, function(res){
//console.log(res);
			if (res.code != 200) {
				mAlert('구매 오류 발생(' + res.code + ')', 'danger', 5000);
				return false;
			} else {
				var type_message = (type != 'permanent') ? '대여' : '구매';
				$('#modal-purchase').modal('hide');
				mAlert(type_message+'가 완료되었습니다. 감사합니다.', 'success', 1500, function(){ 
					if (_self._CART.in_viewer) {
						document.location.href = '/comics/' + _self._CART.item_ids[0];
					} else {
						document.location.reload();
					}
				});
			}
		});
	}
}
