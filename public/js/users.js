/* public/js/users : required jquery, js.cookie */

var Users = new (function(){

// @private

	var _self = this;

	function _request(api, method, body, callback, async) {
		async = async || true;
		body = (!body) ? '' : JSON.stringify(body);
		$.ajax({ url: api, type: method, dataType: 'JSON', contentType: 'application/json', data: body, success: callback, async: async });
	}

// @public

	/* cloud related */
	_self.cloud = {
		login: function(params, callback){
			return _request('/users/login', 'POST', params, callback);
		},

		register: function(data, callback) {
			return _request('/users/register', 'POST', data, callback);
		},

		secession: function(callback) {
			return _request('/users/secession', 'DELETE', null, callback, false);
		},

		resendActivator: function(callback) {
			_request('/users/resend', 'POST', null, callback);
		},

		resetPassword: function(data, callback) {
			_request('/users/password', 'DELETE', data, callback);
		},

		requestCS: function(cs_data, callback) {
			_request('/users/support', 'PUT', cs_data, callback);
		},

		getAdult: function(callback){
			return _request('/users/adult', 'GET', null, callback, false);
		},

		getPoints: function(callback){
			return _request('/users/points', 'POST', null, callback, false);
		},

		getPointStat: function(point_id, callback) {
			_request('/users/points/'+point_id, 'GET', null, callback);
		},

		haveItems: function(callback) {
			return _request('/users/items', 'GET', null, callback);
		},

		registerCoupon: function(coupon_code, callback) {
			return _request('/users/coupon/'+coupon_code, 'PUT', null, callback);
		}
	};

	/* local related */
	_self.local = {
		userInfo: function(callback){
			return _request('/users', 'GET', null, callback, false);
		},

		getPoint: function(points, point_id){
			var point = {};
			points.forEach(function(p){ if (p.points_id == point_id) point = p; });
			return point;
		},

		isLogin: function(){
			if (Cookies.get('access_token')) return true;
			return false;
		},

		toggleAdultView: function(now){
			_request('/users/adult/view', 'POST', {'adult_view': (now == 'yes') ? 'no' : 'yes'}, function(res) {
				document.location.reload();
			});
		},

		haveItems: function(callback) {
			_request('/users/items', 'GET', null, callback);
		}

	}

	this.points = function(callback) { _request('/users/points', 'GET', null, callback); }

	this.logCharged = function(queries, callback) { _request('/users/points/charged?' + queries, 'GET', null, callback); }


	this.actionItems = function(item_id, callback) {
		_request('/users/items/' + item_id, 'GET', null, callback);
	}

	this.recentsItems = function(item_id, callback) {
		_request('/users/items/recents/' + item_id, 'GET', null, callback);
	}

	this.logout = function(callback) {
		Cookies.remove('access_token');
		_request('/users/logout', 'DELETE', null, callback);
	}


	this.getFavorites = function(series, callback) {
		_request('/users/favorites/' + series, 'GET', null, callback);
	}

	this.toggleFavorites = function(series, callback) {
		this.getFavorites(series, function(res) {
			if (res.series_id) {
				_request('/users/favorites/' + res.series_id, 'DELETE', null, callback('remove'));
			} else {
				_request('/users/favorites/' + series, 'POST', null, callback('add'));
			}
		});
	}

	this.getReceipts = function(callback) {
		_request('/users/purchases', 'POST', null, callback);
	}

	this.buyItem = function(product_id, points_id, callback) {
		_request('/users/buy', 'POST', {'product_id': product_id, 'points_id': points_id || 'O_0000_000000009'}, callback);
	}

	this.buyItems = function(product_ids, points_id, callback) {
		_request('/users/buys', 'POST', {'product_ids': product_ids, 'points_id': points_id || 'O_0000_000000009'}, callback);
	}

	this.verifyAdult = function(callback) {
		_request('/users/adult', 'GET', null, callback);
	}

	this.submitEvent = function(event_id, callback) {
		_request('/users/event/'+event_id, 'POST', null, callback);
	}

});

/* extra interface --------------------------------------------------------- */

/* default login */

function defaultLogin(e) {
	e.preventDefault();

	if (!$('#user-email').val()) {
		mAlert('이메일 주소를 입력해주세요.', 'danger');
		$('#user-email').focus(); return;
	}

	if (!$('#user-passwd').val()) {
		mAlert('비밀번호를 입력해주세요.', 'danger');
		$('#user-passwd').focus(); return;
	}

	var userData = {
		'channel': $('#channel').val(),
		'email': $('#user-email').val(),
		'password': $('#user-passwd').val()
	}

	Users.cloud.login(userData, function(res){
		if (res.access_token) {
			if (res.cloud.activated != 'yes') {
				mConfirm.set({
					'onConfirm': function(opts){
						Users.cloud.resendActivator(function(res){
							mAlert( '인증 이메일이 발송 되었습니다. 확인 부탁드립니다.', 'success', 3000, usersLogout);
						});
					},
					'onCancel': function(opts) {
						usersLogout();
					}
				}).show('가입확인 이메일을 통해 인증되지 않은 상태입니다. 다시 인증 이메일을 받으시겠습니까?');
			} else {
				// success login process
				Cookies.remove('access_token');
				Cookies.set('access_token', res.access_token, { expires: 1/24 }); // 1 hours

				if ($('#remember_login').prop('checked'))
					Cookies.set('access_token', res.access_token, { expires: 90 }); // 90 days

				mAlert(
					'로그인 중...'
					,'success' ,2000
					,function(){ var rurl = decodeURIComponent($('#rurl').val()); goUrl(rurl); }
				);
				return true;
			}
		} else {
			mAlert('아이디 혹은 비밀번호가 일치하지 않습니다. 확인 후 다시 입력해주세요.', 'danger');
			return false;
		}
	});
}

/* */

function usersLogout() {
	Users.logout(function() { goUrl('/'); });
}

function loadUserInfo() {
	Users.local.userInfo(function(user){
		if (user) {
			if (!user.points || !user.is_adult) {
				Users.cloud.getPoints(function(rp) {
					user.points = rp.points;
					var base_point = Users.local.getPoint(user.points, 'O_0000_000000009');
					if (base_point.points_info) $('#user-points').text(withCommas(base_point.points_info.total_amount));

					if (!user.is_adult) {
						Users.cloud.getAdult(function(ra) {
							user.is_adult = ra.is_adult;
console.log('restore-login', user);
							Catalogs.renderIn($('#nav-adult-check'), '/r/nav/adult-a', {'data': user});
						});
					} else {
						Catalogs.renderIn($('#nav-adult-check'), '/r/nav/adult-a', {'data': user});
					}
				});
			} else {
				var base_point = Users.local.getPoint(user.points, 'O_0000_000000009');
				if (base_point.points_info) $('#user-points').text(withCommas(base_point.points_info.total_amount));
				Catalogs.renderIn($('#nav-adult-check'), '/r/nav/adult-a', {'data': user});
			}
		} else {
			Catalogs.renderIn($('#nav-adult-check'), '/r/nav/adult-a', {'data': false});
		}
	});
}

function toggleFavorites(el, series) {
	Users.toggleFavorites(series, function(action){
		if (action == 'add') {
			mAlert('즐겨찾기에 추가되었습니다.', 'success');
			$(el).find('.fa').removeClass('fa-heart-o').addClass('fa-heart');
		}
		if (action == 'remove') $(el).find('.fa').removeClass('fa-heart').addClass('fa-heart-o');
	});
}

function checkLoginAgreements() {
	if ($('.agreements:checked').length < 2) {
		mAlert('[필수] 약관에 동의하셔야 로그인이 가능합니다.', 'danger');
		return false;
	}
	return true;
}

function checkAdult() {
	Users.info(function(user) {
		if (!user || (user.is_adult != 'yes')) {
			alert("해당 작품은 성인인증을 필요로 하는 컨텐츠 입니다.\n성인 인증(로그인 필요) 후 보시기 바랍니다.", 'info');
			window.history.back();
		}
	});
	return;
}

$(document).ready(function(){
	loadUserInfo();
});
