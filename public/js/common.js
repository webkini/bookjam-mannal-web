/* js/common.js in static public */

$(function(){
	/* BASIC UI */
	$('[data-toggle="popover"]').popover();
	$('[data-toggle="tooltip"]').tooltip();
	$('.scroll-top-wrapper').on('click', scrollToTop);

})
/* SCROLL TO TOP */
.on('scroll', function(e){
	if ($(window).scrollTop() > 100) {
		$('.scroll-top-wrapper').addClass('show');
	} else {
		$('.scroll-top-wrapper').removeClass('show');
	}
})
/* LOAD SPINNER, LAZYLOAD */
.ajaxStop(function(){
	$('#spinner-box').hide();
	//$('img.cover').lazyload({ effect: "fadeIn" });
	$('.row-fluid').each(function(i,c){
		$('img.cover').lazyload({ effect: "fadeIn", container: $(this) });
	});
})
.ajaxStart(function(){ $('#spinner-box').show(); })
.ajaxError(function(){ $('#spinner-box').hide(); });
//.ajaxComplete(function(){ $('#spinner-box').hide(); });

function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 400, 'linear');
}

/* HELPERS */

function getDateFormat(timestamp, format) {
	var d = new Date(parseInt(timestamp) * 1000);
	format = format + '';
	format = format.replace('Y', d.getFullYear());
	format = format.replace('y', d.getFullYear().toString().substr(2,2));
	format = format.replace('m', padZero(d.getMonth() + 1,2));
	format = format.replace('d', padZero(d.getDate(),2));
	format = format.replace('h', padZero(d.getHours(),2));
	format = format.replace('i', padZero(d.getMinutes(),2));
	format = format.replace('s', padZero(d.getSeconds(),2));
	return format;
}

function withCommas(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function padZero(value, length) {
	var b = Math.pow(10, length),
		v = parseInt(value);
	return String(b + v).substring(1);
}

function mergeObjects(a,b) {
	for (var k in b) a[k] = b[k];
	return a;
}

function goUrl(url) {
	document.location.href = url;
}

function requiredLogin() {
	mConfirm.set({
		'onConfirm': function(){
			goUrl('/login?rurl=' + encodeURIComponent(document.location.href));
		}
	}).show('로그인을 필요로 하는 기능입니다. 로그인 하시겠습니까?');
}

function mAlert(message, type, delay, after_cb) {
    type = type || 'info';
    delay = delay || 3000;

	if (type == 'info') message = '<i class="fa fa-commenting" aria-hidden="true"></i> ' + message;
	if (type == 'danger') message = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' + message;
	if (type == 'success') message = '<i class="fa fa-check" aria-hidden="true"></i> ' + message;

	var offsetAmount = parseInt(window.innerHeight / 2) - 30;
//console.log(offsetAmount);

	$.bootstrapGrowl(message, {
  		ele: 'body',
  		type: type, // null, info, danger, success
  		offset: {from: 'top', amount: offsetAmount}, // 'top', or 'bottom'
  		align: 'center',
  		width: 'auto',
  		delay: delay,
		stackup_spacing: 10,
        allow_dismiss: true
	});

	if (after_cb) setTimeout(after_cb, delay);
}

/* GOOGLE ANALYTICS */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-30027224-10', 'auto');
ga('send', 'pageview');
ga('require', 'ec');

window.addEventListener("load",function() {
setTimeout(function(){ window.scrollTo(0, 1); }, 0);
});
