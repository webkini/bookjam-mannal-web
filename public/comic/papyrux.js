
if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function(str) {
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.endsWith != 'function') {
    String.prototype.endsWith = function(str) {
        return this.slice(-str.length) == str;
    };
}

if (typeof btoa != 'function') {
    btoa = function(s) {
        var padchar = '=';
        var alpha   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        var getbyte = function(s, i) {
            return s.charCodeAt(i);
        };

        var i, b10;
        var x = [];

        // convert to string
        s = '' + s;

        var imax = s.length - s.length % 3;

        if (s.length === 0) {
            return s;
        }
        for (i = 0; i < imax; i += 3) {
            b10 = (getbyte(s,i) << 16) | (getbyte(s,i+1) << 8) | getbyte(s,i+2);
            x.push(alpha.charAt(b10 >> 18));
            x.push(alpha.charAt((b10 >> 12) & 0x3F));
            x.push(alpha.charAt((b10 >> 6) & 0x3f));
            x.push(alpha.charAt(b10 & 0x3f));
        }
        switch (s.length - imax) {
        case 1:
            b10 = getbyte(s,i) << 16;
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) +
                   padchar + padchar);
            break;
        case 2:
            b10 = (getbyte(s,i) << 16) | (getbyte(s,i+1) << 8);
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) +
                   alpha.charAt((b10 >> 6) & 0x3f) + padchar);
            break;
        }
        return x.join('');
    }
}

// modules
function buildModules() {
    sbml = {}
    Papyrus = {}

    ////////////////////////////////////////////////////////////////////////////////
    // module: sbml, class: *

    sbml = Module
    // {
    //     PropMap: Module.PropMap,
    //     Logger: Module.Logger,
    //     Font: Module.Font,
    //     FontFactory: Module.FontFactory,
    //     ImageRegistry: Module.ImageRegistry,
    //     BookRenderer: Module.BookRenderer,
    //     BookRendererDelegate: Module.BookRendererDelegate,
    //     BookCompiler: Module.BookCompiler,
    //     BookCompilerDelegate: Module.BookCompilerDelegate
    // }

    // sbml.PropMap              = Module.PropMap;
    // sbml.Logger               = Module.Logger;
    // sbml.Font                 = Module.Font;
    // sbml.FontFactory          = Module.FontFactory;
    // sbml.ImageRegistry        = Module.ImageRegistry;
    // sbml.BookRenderer         = Module.BookRenderer;
    // sbml.BookRendererDelegate = Module.BookRendererDelegate;
    // sbml.BookCompiler         = Module.BookCompiler;
    // sbml.BookCompilerDelegate = Module.BookCompilerDelegate;

    ////////////////////////////////////////////////////////////////////////////////
    // module: Papyrus, class: FontFactory

    Papyrus.FontFactory = function(context) {
        this.impl = new sbml.FontFactory({
            createFont: function(family, size) {
                if (family == '') {
                    family = 'serif';
                } else if (family.startsWith('RixGo')) {
                    family = 'sans-serif';
                    size *= 1.1;
                } else if (family.startsWith('OpenBooks')) {
                    family = 'serif';
                }
                var font_spec = (12.5 * size + 0.05).toFixed(1) + 'pt ' + family;
                var ascent = null;
                var descent = null;
                return new sbml.Font(size, {
                    spec: font_spec,
                    ascent: function() {
                        if (ascent == null) ascent = this.measureText('한') * 0.9;
                        return ascent;
                    },
                    descent: function() {
                        if (descent == null) descent = this.measureText('한') * -0.2;
                        return descent;
                    },
                    measureText: function(text) {
                        context.font = this.spec;
                        return context.measureText(text).width;
                    }
                });
            } // end of createFont
        });
        this.context = context;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // module: Papyrus, class: ImageRegistry

    Papyrus.ImageRegistry = function(deviceSpec) {
        var impl = new sbml.ImageRegistry(deviceSpec);
        this.reset = function() { impl.reset(); }
        this.addFileName = function(fileName) { impl.addFileName(fileName); }
        this.selectImage = function(imageName) { return impl.selectImage(imageName); }
        this.getFileName = function(imageName) {
            var imageSpec = impl.selectImage(imageName);
            if (imageSpec != null) return imageSpec.name;
            return null;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // module: Papyrus, class: Book

    Papyrus.Book = function(impl, bxp, fontFactory, imageRegistry) {

        function createImageURL(name) {
            var fileName = imageRegistry.getFileName(name);
            if (fileName != null) {
                var ext = fileName.substring(fileName.length - 3);
                var blob = bxp.file('Images/' + fileName).asBinary();
                return "data:image/" + ext + ";base64," + btoa(blob);
            }
            return null;
        }

        this.drawPage = function(context, pageIndex) {
            // one page draw
            var page = impl.getPageAt(pageIndex);
            var boxCount = page.getBoxCount();
            var objectBoxes = [];
            for(var i=0; i < boxCount; i++) {
                if (page.getBoxAt(i).type == 'object')
                    objectBoxes.push(page.getBoxAt(i));
            }
            objectBoxes.forEach(function(ob) {
                context.clearRect(0, 0, context.canvas.width, context.canvas.height);
                // 컨텍스트 리셋
                context.beginPath();
                var img = new Image();
                /*
                img.onload = function() {
                    context.drawImage(img,
                        0, 0, context.canvas.width, context.canvas.height);
                };
                */
                img.src = 'http://store.bookjam.pe.kr/comics/CM_DCW_0177_000001/Images/' + ob.getProperty('image');
                $('.recto').html('<img src="'+img.src+'" />');
            });
        }

        this.drawPageCom = function(context, pageIndex) {
            var rendererDelegate = new sbml.BookRendererDelegate({
                drawText: function(text, rect, font, color) {
                    context.font = font.spec;
                    context.fillStyle = color;
                    context.textBaseline = 'alphabetic';
                    context.fillText(text, rect.left, rect.top + font.ascent());
                },
                fillRect: function(rect, color) {
                    context.fillStyle = color;
                    context.fillRect(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
                },
                drawImage: function(name, rect, method) {
                    var url = createImageURL(name);
                    if (url != null) {
                        var img = new Image;
                        img.src = url;
                        var x = rect.left;
                        var y = rect.top;
                        var w = rect.right - rect.left;
                        var h = rect.bottom - rect.top;
                        if (method == 'pattern') {
                            context.rect(x, y, w, h);
                            context.fillStyle = context.createPattern(img, 'repeat');;
                            context.fill();
                        } else {
                            context.drawImage(img, x, y, w, h);
                        }
                    }
                }
            });
            var renderer = new sbml.BookRenderer(rendererDelegate, fontFactory.impl);
            renderer.drawPage(impl, pageIndex);
        }

        this.getPageCount = function() {
            return impl.getPageCount();
        }
    }


    ////////////////////////////////////////////////////////////////////////////////
    // module: Papyrus, class: BookCompiler

    Papyrus.BookCompiler = function(fontFactory, imageRegistry) {

        this.compile = function(bxp, layoutName) {
            imageRegistry.reset();

            //for (var name in bxp.folder('Images').files) {
            for (var name in bxp.files) {
                if (bxp.files[name].images) {
                    name = name.substring(7); // remove "Images/"
                    imageRegistry.addFileName(name);
                }
            }

            var imap = bxp.imap; //bxp.file('imap.bon').asText();

            function getImageSize(imageName) {
                var imageSpec = imageRegistry.selectImage(imageName);

                if (imageSpec != null) {
                    var fromIndex = imap.indexOf(imageSpec.name) + imageSpec.name.length;
                    var m = imap.substring(fromIndex).match(/(\d+)\s*,\s*(\d+)/);

                    if (m != null) {
                        var screenScale = Papyrus.getScreenScale(fontFactory.context);
                        var width  = parseInt(m[1]) / imageSpec.scale / screenScale;
                        var height = parseInt(m[2]) / imageSpec.scale / screenScale;
                        return [width, height];
                    }
                }

                return null;
            }

            var delegate = new sbml.BookCompilerDelegate({

                loadFile: function(path) {
                    //return bxp.file(path).asBinary();
                    return bxp[path];
                },

                getObjectSize: function(type, props) {
                    if (type == 'sbml:image') {
                        var size = getImageSize(props.get('filename'));
                        if (size != null)
                            return size;
                    }

                    return [100, 100];
                }
            });

            var logger = new sbml.Logger({
                logLine: function(level, message) {
                    //console.log(message);
                }
            });

            var envVar = new sbml.PropMap();

            if (layoutName.startsWith("iphone"))
                envVar.set("DEVICE", "iPhone");

            if (layoutName.startsWith("ipad"))
                envVar.set("DEVICE", "iPad");

            if (layoutName.endsWith('protrait'))
                envVar.set("ORIENTATION", "protrait");

            if (layoutName.endsWith('landscape'))
                envVar.set("ORIENTATION", "landscape");

            var compImpl = new sbml.BookCompiler(delegate, fontFactory.impl, logger);
            var bookImpl = compImpl.compile(layoutName, envVar);

            /*
            var firstPage = bookImpl.getPageAt(1);
            for (i = 0; i < firstPage.getBoxCount(); ++i) {
                if (typeof firstPage.getBoxAt(i) == 'object') {
                    console.log('book', i, firstPage.getBoxAt(i));
                    if (firstPage.getBoxAt(i).type == 'object'){}
                }
            }
            console.log('prop', firstPage.getBoxAt(3).getProperty('image'));
            */
	        if (bookImpl != null) {
                return new Papyrus.Book(bookImpl, bxp, fontFactory, imageRegistry);
            }

        return null;
        }
    }


    ////////////////////////////////////////////////////////////////////////////////
    // module: Papyrus, function: getScreenScale()
    Papyrus.getScreenScale = function(context) {
        if (this.__screenScale == null) {
            var devicePixelRatio = window.devicePixelRatio || 1;
            var backingStoreRatio = context.webkitBackingStorePixelRatio ||
                                    context.mozBackingStorePixelRatio ||
                                    context.msBackingStorePixelRatio ||
                                    context.oBackingStorePixelRatio ||
                                    context.backingStorePixelRatio || 1;
            this.__screenScale = devicePixelRatio / backingStoreRatio;
        }
        return this.__screenScale;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // module: Papyrus, function: setupCanvas()
    Papyrus.setupCanvas = function(canvas) {
        var context = canvas.getContext('2d');
        var screenScale = this.getScreenScale(context);
        if (screenScale != 1) {
            var oldWidth = canvas.width;
            var oldHeight = canvas.height;
            canvas.width = oldWidth * screenScale;
            canvas.height = oldHeight * screenScale;
            canvas.style.width = oldWidth + 'px';
            canvas.style.height = oldHeight + 'px';
            context.scale(screenScale, screenScale);
        }
        return context;
    }
} // end of buildModules


// bootstrap
function PapyrusMain(callback) {
    buildModules();
    callback();
}
