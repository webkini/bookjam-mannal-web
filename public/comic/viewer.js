var VIEWER = {
	'mode': { 'preview': true, 'mobile': false, 'orientation': 'portrait', 'direction': 'ltr' },
	'page': { 'current': 0, 'total': 0, 'sides': 1 },
	'item': {}
};

var BOOK, CONTEXT, HAVE_ITEMS = [];

function loadItemInfo(item_id, callback) {
	if (item_id != '') {
		Catalogs.itemsMeta([item_id], function(res){
      		var item = res.items[item_id];
//console.log(item);
			VIEWER.item = mergeObjects(item, VIEWER.item);
			VIEWER.mode.direction = item['page-direction'] || 'ltr';

			/* setting head menu */
			$('#series-home').attr('href', '/series/'+ item.series[0] +'/detail');
			$('#item-title').text(item.title);

			/* volume controller */
			if (item['prev-item']) {
				$('.go-prev-vol').attr('data-item', item['prev-item']);
				if (HAVE_ITEMS.indexOf(item['prev-item']) > -1) {
					$('.go-prev-vol').attr('href', '/comic/'+item['prev-item']);
				} else {
					$('.go-prev-vol').click(function(e){
						mConfirm.set({
							'onConfirm': function() {
								Store.openCart('single', [item['prev-item']], 'O_0000_000000009', true);
							}
						}).show('이전편을 구매하지 않으셨습니다.<br/>구매하시겠습니까?');
					});
				}
			}

			if (item['next-item']) {
				$('.go-next-vol').attr('data-item', item['next-item']);
				if (HAVE_ITEMS.indexOf(item['next-item']) > -1) {
					$('.go-next-vol').attr('href', '/comic/'+item['next-item']);
				} else {
					$('.go-next-vol').click(function(e){
						mConfirm.set({
							'onConfirm': function() {
								Store.openCart('single', [item['next-item']], 'O_0000_000000009', true);
							}
						}).show('다음편을 구매하지 않으셨습니다.<br/>구매하시겠습니까?');
					});
				}
			}

			if (VIEWER.mode.preview) {
				$('.go-vol').remove();
			} else {
				$('.go-vol').each(function(){ if ($(this).attr('data-item') == '') $(this).remove(); });
			}

      /* page direction set */
			$('#page-slider').parent('div').attr('dir', VIEWER.mode.direction);
			$('#page-slider').slider().on('slideStop', function(o){
				VIEWER.page.current = o.value;
				PageMove();
			});

			if (VIEWER.mode.direction == 'rtl') {
				$('.go-next-page').insertBefore($('.show-context-menu'));
				$('.go-prev-page').insertAfter($('.show-context-menu'));
				$('.fa-chevron-right').hide();

				var prev_vol = $('.go-prev-vol').first();
				var next_vol = $('.go-next-vol').first();
				$('.left-vol').html(next_vol);
				$('.right-vol').html(prev_vol);
			} else {
				$('.fa-chevron-left').hide();
			}

			Catalogs.seriesItems(item.series, '', function(res){
				var BASE_ITEMS = [VIEWER.item.id];
				var RELATED_ITEMS = [];

				if (VIEWER.item['prev-item']) BASE_ITEMS.push(VIEWER.item['prev-item']);
				if (VIEWER.item['next-item']) BASE_ITEMS.push(VIEWER.item['next-item']);

				res.items.forEach(function(item){
					if (BASE_ITEMS.indexOf(item.id) > -1) RELATED_ITEMS.push(item);
				});

				Catalogs.renderIn($('#related-items'), '/r/item/list-a', {'data': RELATED_ITEMS}, function(){
					if (callback) callback(item);
				});
			});

		});
	}
}

function loadBook(item) {
	if (VIEWER.mode.mobile) {
		toggleFullScreen();
	}

	var contextCanvas = Papyrus.setupCanvas(document.getElementById('context-canvas'));
	contextCanvas.canvas.height = VIEWER.item.height;
	contextCanvas.canvas.width  = VIEWER.item.width;
	CONTEXT = [contextCanvas];

	var DEVICE_SPEC = (VIEWER.mode.mobile) ? 'fxl2@x' : 'pxl2@x';
	Papyrus.getScreenScale(CONTEXT[0]);

	loadComicBxp(item.id, function(bxp) {
		var ff = new Papyrus.FontFactory(CONTEXT[0]);
		var ir = new Papyrus.ImageRegistry(DEVICE_SPEC);
		var bc = new Papyrus.BookCompiler(ff, ir);

		BOOK = bc.compile(bxp, 'iphone-portrait');
		if (BOOK != null) {
			VIEWER.page.total = (VIEWER.mode.preview) ? 10 : parseInt(BOOK.getPageCount());
			setPageText(1, VIEWER.page.total);
			redrawPages();
		}
	});
}

function loadComicBxp(item_id, callback) {
	var BXP = {"item": item_id, "files": {}};
	var REMOTE_HOST = 'http://store.bookjam.pe.kr/comics/' + item_id;

	function _loadItemFile(filename, callback) {
		$.ajax({
			url: '/catalogs/items/files',
			type: 'POST',
			data: {"url": REMOTE_HOST + '/' + filename},
			success: callback
		});
	}

	loadImapBon();

	function loadImapBon() {
		_loadItemFile('imap.bon', function(file) {
			if (file.indexOf('404 Not Found') > -1) {
				mAlert('파일을 찾을 수 없습니다.', 'danger', 5000, function(){
					history.back();
				});
				return false;
			}
			BXP["imap.bon"] = file;
			BXP["imap.obj"] = JSON.parse(file.replace(/([a-z][^:]*)(?=\s*:)/g, '"$1"')); // loose json parse
			for (key in BXP["imap.obj"]) { var name = 'Images/' + key; BXP.files[name] = { 'image': true }; }
			loadBookBon();
		});
	}
	function loadBookBon() { _loadItemFile('book.bon', function(file) { BXP["book.bon"] = file; loadPagesSbss(); }); }
	function loadPagesSbss() { _loadItemFile('pages.sbss', function(file) { BXP["pages.sbss"] = file; loadPagesSbml(); }); }
	function loadPagesSbml() { _loadItemFile('pages.sbml', function(file) { BXP["pages.sbml"] = file; callback(BXP); }); }
}

function redrawPages() {
	var page = VIEWER.page, item = VIEWER.item;
	var pageNo = page.current;
	var IMAGE_HOST = 'http://store.bookjam.pe.kr/comics/'+ item.id +'/Images/';

	scrollTop();
	$('.view-page').hide();
	var PAGE_SIDE = ['.recto', '.verso'];
	if (VIEWER.mode.direction == 'rtl') PAGE_SIDE = ['.verso', '.recto'];

	//CONTEXT[0].canvas.height = item.height;
	//CONTEXT[0].canvas.width = item.width;

	// precached pages
	var pageRange = {
		'first': (pageNo < 2) ? 0 : parseInt(pageNo - 2),
		'last': parseInt(pageNo + 2)
	}
	$('#cached-pages').empty();
	for (var i = pageRange.first; pageRange.last >= i; i++) {
		var images = BOOK.drawPage(CONTEXT[0], i);
		$('#cached-pages').append('<img src="'+ IMAGE_HOST + images[0] +'">');
	}

	// load page
	for (var i = 0; i < page.sides; i++) {
		var images = BOOK.drawPage(CONTEXT[0], pageNo + i);
		var margin_top = parseInt((page.height - item.height) / 2) + 'px';

		if (VIEWER.mode.orientation == 'landscape') {
			margin_top = 0;
			$('.pages-container').css('overflow', 'auto');
		} else {
			$('.pages-container').css('overflow', 'hidden');
		}

		$(PAGE_SIDE[i]).html('<img src="'+ IMAGE_HOST + images[0] +'">');
		$(PAGE_SIDE[i] +' img').height(item.height).css('margin-top', margin_top);
		$(PAGE_SIDE[i]).css('display','inline-block');
	}

	$('#page-slider').slider({ step:page.sides, min:1, max:page.total });
}

function resizeCanvas(callback) {
	var orientation = VIEWER.mode.orientation;
	var page = VIEWER.page, item = VIEWER.item;

	page.ratio  = window.devicePixelRatio || 1;
	page.height = window.innerHeight || document.body.clientHeight;
	page.width  = window.innerWidth  || document.body.clientWidth;

	var image = { 'width': 1080, 'height': 1560 };

	var I_R = parseFloat((image.width * page.sides) / image.height),
		S_R = parseFloat(page.width / page.height);

	if ((orientation != 'landscape') && (S_R > I_R)) { // height 우선
		item.height = page.height;
		item.width  = parseInt(item.height * I_R);
	} else {
		item.width  = page.width;
		item.height = parseInt(item.width / I_R);
	}

	var top_margin = parseInt((page.height - item.height) / 2);
	if (top_margin < 0) top_margin = 0;

	var view_page_width  = parseInt(100 / page.sides) + '%';
	var ctx_canvas_width = parseInt(item.width / page.sides) + 'px';

	$('.pages-control').height(item.height);
	$('.view-page').css('width', view_page_width).css('height', '100%');
	$('#context-canvas').attr('width', ctx_canvas_width).attr('height', page.height);

	if (callback) callback();
}

function setPageText(current, total) {
	$('#total-pages').html(total);
	$('#current-page').html(current);
	$('#current-page-ratio').html('('+ Math.ceil(current/total*100) +'%)');
}

function PageMove() {
	setTimeout(function(){ $('.context-menu').hide(); }, 700);
	var page = VIEWER.page;
	var selectPage = parseInt(page.current);

	if (selectPage <= 0) {
		mAlert('첫 페이지로 이동합니다.', 'info');
		selectPage = 1;
	}

	if (selectPage > page.total) {
		mAlert('마지막 페이지로 이동합니다.', 'info');
		selectPage = page.total;
	}

	page.current = selectPage - page.sides;
	setPageText(selectPage, page.total);
	redrawPages();
}

function PageControl(move) {
	setTimeout(function(){ $('.context-menu').hide(); }, 700);
  	var page = VIEWER.page;
	var selectPage = page.current + (move * page.sides);
	var autoMoveIns, autoMoveUrl, redraw = true;

	if (selectPage < 0) {
		selectPage = 0;
		_prevPage();
		redraw = false;
	}

	if (selectPage >= page.total) {
		selectPage = (page.sides > 1) ? (page.total - 1) : (page.total - page.sides);
		_lastPage();
		redraw = false;
	}

	VIEWER.page.current = selectPage;
	setPageText(selectPage + 1, page.total);
	$("#page-slider").slider('setValue', selectPage + 1);
	if (redraw) redrawPages();

	function _lastPage() {
		if (VIEWER.mode.preview) {
			mConfirm.set({
				'beforeShow': function() { $('#modal-confirm .confirm').html('나가기'); },
				'onConfirm': function() { goUrl('/series/'+ VIEWER.item.series[0] +'/detail'); }
			}).show('미리보기 마지막 페이지 입니다.<br/>더 읽으시려면 정식판을 구매해주세요.');
		} else {
			if (VIEWER.item['next-item']) {
				if (HAVE_ITEMS.indexOf(VIEWER.item['next-item']) > -1) {
					mConfirm.set({
						'beforeShow': function() {
							$('#modal-confirm .confirm').html('이동');
							autoMoveIns = setInterval(_autoMove, 1000);
							autoMoveUrl = '/comic/'+ VIEWER.item['next-item'];
						},
						'onConfirm': function() { goUrl('/comic/'+ VIEWER.item['next-item']); }
					}).show('마지막 페이지 입니다.<br/>다음편으로 이동하시겠습니까?<br/><span id="autoMoveCount">3</span>초 뒤에 자동으로 이동합니다.');
				} else {
					mConfirm.set({
						'onConfirm': function() {
							Store.openCart('single', [VIEWER.item['next-item']], 'O_0000_000000009', true);
						}
					}).show('마지막 페이지 입니다.<br/>다음편을 구매하시겠습니까?');
				}
			}
		}
	}

	function _prevPage() {
		if (VIEWER.mode.preview || !VIEWER.item['prev-item']) {
			mAlert('첫 페이지 입니다.', 'info', 1000);
			return;
		}

		if (VIEWER.item['prev-item']) {
			if (HAVE_ITEMS.indexOf(VIEWER.item['prev-item']) > -1) {
				mConfirm.set({
					'beforeShow': function() {
						$('#modal-confirm .confirm').html('이동');
						autoMoveIns = setInterval(_autoMove, 1000);
						autoMoveUrl = '/comic/'+ VIEWER.item['prev-item'];
					},
					'onConfirm': function() { goUrl('/comic/'+ VIEWER.item['prev-item']); }
				}).show('첫 페이지 입니다.<br/>이전편으로 이동하시겠습니까?<br/><span id="autoMoveCount">10</span>초 뒤에 자동으로 이동합니다.');
			} else {
				mConfirm.set({
					'onConfirm': function() {
						Store.openCart('single', [VIEWER.item['prev-item']], 'O_0000_000000009', true);
					}
				}).show('첫 페이지 입니다.<br/>이전편을 구매하시겠습니까?');
			}
		}
	}

	function _autoMove() {
		var remain = parseInt($('#autoMoveCount').text());
		if (remain > 0) {
			$('#autoMoveCount').text(--remain);
		} else {
			clearInterval(autoMoveIns);
			goUrl(autoMoveUrl);
		}
	}
}

function scrollTop() {
	$('.pages-container').scrollTop(0)
}

function toggleMenu() {
	var $cm = $('.context-menu');
	if ($cm.css('display') != 'block') {
		$cm.css('display', 'block');
	} else {
		$cm.css('display', 'none');
	}
}

function toggleTwoSides() {
	$('.context-menu').hide();
  	var page = VIEWER.page;
	var selectPage = page.current;
	if (selectPage % 2) selectPage--;
	VIEWER.page.current = selectPage;
	if (page.sides > 1) {
		VIEWER.page.sides = 1;
		$('#page-sides').find('i').removeClass('fa-window-maximize').addClass('fa-columns').attr('title','2면보기');
	} else {
		VIEWER.page.sides = 2;
		$('#page-sides').find('i').removeClass('fa-columns').addClass('fa-window-maximize').attr('title','1면보기');;

	}
	resizeCanvas(function(){
		setPageText(selectPage + 1, page.total);
		$('#page-slider').slider({ step: page.sides, min:1, max: page.total, 'setValue': selectPage + 1 });
		redrawPages();
	});
}

function toggleFullScreen(mode) {
	if (mode ||
		(document.fullScreenElement && document.fullScreenElement !== null) ||
   		(!document.mozFullScreen && !document.webkitIsFullScreen))
	{
		if (document.documentElement.requestFullScreen) {
			document.documentElement.requestFullScreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullScreen) {
			document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}
  	} else {
    	if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	}
}
